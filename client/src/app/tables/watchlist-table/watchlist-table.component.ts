import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TableService} from '../tables.service';
import {fromEvent as observableFromEvent, BehaviorSubject} from 'rxjs';
import {distinctUntilChanged, debounceTime, switchMap} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../core/services/user.service';
import {MatMultiSort} from 'ngx-mat-multi-sort';
import * as _ from 'lodash';

@Component({
    selector: 'app-watchlist-table',
    templateUrl: './watchlist-table.component.html',
    styleUrls: ['./watchlist-table.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class WatchlistTableComponent implements OnInit, AfterViewInit {
    dataSource: any;
    fundTableForm: FormGroup;
    headerNames: Array<string> = ['Restaurant_Name', 'Cuisines', 'Average_Cost_for_two', 'Has_Table_booking', 'Has_Online_delivery', 'Aggregate_rating', 'Votes'];
    columnsToDisplay: any;
    selectedData: any;
    showList = false;
    showTable = true;

    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatMultiSort, {static: false}) multiSort: MatMultiSort;
    @ViewChild('filter', {static: false}) filter: ElementRef;

    constructor(private readonly tableService: TableService, private formBuilder: FormBuilder,
                private userService: UserService) {
    }

    ngOnInit(): void {
        this.fundTableForm = this.formBuilder.group({
            fundSchemeName: new FormControl('', Validators.required)
        });
    }

    ngAfterViewInit(): void {
        this.tableService.fetchData().subscribe((res) => {
            res.data.map(function (item) {
                delete item['_id'];
                return item;
            });

            this.dataSource = new MatTableDataSource(res.data);
            this.columnsToDisplay = res.data;
            this.dataSource.paginator = this.paginator;
            // this.dataSource.sort = this.sort;
            this.dataSource.sort = this.multiSort;
            this.dataSource.sortData = this.sortData;
            this.dataSource.sortingDataAccessor = this.sortingDataAccessor;

            observableFromEvent(this.filter.nativeElement, 'keyup').pipe(
                debounceTime(150),
                distinctUntilChanged(), )
                .subscribe(() => {
                    if (!this.dataSource) {
                        return;
                    }
                    this.dataSource.filter = this.filter.nativeElement.value;
                });
        });
    }

    private getFundRawData({value, valid}: { value: any, valid: boolean }) {
        return this.fundTableForm.getRawValue();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    getSelectedData(data) {
        this.showList = true;
        this.showTable = false;

        this.tableService.fetchRestaurantData(data.Restaurant_ID).subscribe((res) => {
            if (res.data[0]._id) {
                delete res.data[0]._id;
            } else if (res.data[0]['Restaurant ID']) {
                delete res.data['0']['Restaurant ID'];
            }
            this.selectedData = res.data[0];
        });
    }

    sortData(data: any, sort: MatMultiSort): any {
        const actives = sort.actives.map(headerId => _.partialRight(this.sortingDataAccessor, headerId));
        const directions = sort.directions as 'asc' | 'desc'[];
        return _.orderBy(data, actives, directions);
    }

    sortingDataAccessor(data: any, sortHeaderId: string): string | number {
        const value = (data as { [key: string]: any })[sortHeaderId];
        switch (sortHeaderId) {
            case 'Aggregate_rating':
                return Number(value);
            case 'Average_Cost_for_two':
                return Number(value);
            case 'Votes':
                return Number(value);
            default:
                return Number(value);
        }
    }

    getBackToTable() {
        this.showList = false;
        this.showTable = true;
    }
}

