import {Component, OnInit, Input, OnChanges, AfterViewInit, OnDestroy} from '@angular/core';
import {MediaChange} from '@angular/flex-layout';
import {UserService} from '../core/services/user.service';
import {CommonService} from '../core/common.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @Input() isVisible = true;
  visibility = 'shown';

  sideNavOpened = true;
  matDrawerOpened = false;
  matDrawerShow = true;
  sideNavMode = 'side';

  private media;

  /** Collections of subscribed variables */
  subscriptions: Subscription[] = [];

  ngOnChanges() {
    this.visibility = this.isVisible ? 'shown' : 'hidden';
  }

  constructor(private userService: UserService, private commonService: CommonService) {
  }

  ngOnInit() {
    this.media.subscribe((mediaChange: MediaChange) => {
      this.toggleView();
    });
  }

  ngAfterViewInit(): void {
  }

  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.animation;
    // return outlet.isActivated ? outlet.activatedRoute : ''
  }

  toggleView() {
    if (this.media.isActive('gt-md')) {
      this.sideNavMode = 'side';
      this.sideNavOpened = true;
      this.matDrawerOpened = false;
      this.matDrawerShow = true;
    } else if (this.media.isActive('gt-xs')) {
      this.sideNavMode = 'side';
      this.sideNavOpened = false;
      this.matDrawerOpened = true;
      this.matDrawerShow = true;
    } else if (this.media.isActive('lt-sm')) {
      this.sideNavMode = 'over';
      this.sideNavOpened = false;
      this.matDrawerOpened = false;
      this.matDrawerShow = false;
    }
  }

  /* called when component is being destroyed
     * clean memory or unsubscribe to current events to avoid memory leaks */
  ngOnDestroy () {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
