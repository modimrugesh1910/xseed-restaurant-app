import {RouterModule, Routes} from '@angular/router';
import {AuthComponent} from './auth.component';

export const authRoutes: Routes = [{
  path: '', component: AuthComponent, children: [
    {path: '', redirectTo: 'table', pathMatch: 'full'},
    {path: 'table', loadChildren: '../tables/tables.module#TablesModule'},
  ]
}];
