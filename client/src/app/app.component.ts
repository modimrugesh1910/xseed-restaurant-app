import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor() {
  }

  ngOnInit() {
    // storing user’s address via asking for location permission and storing it in session-storage for further use
    navigator.geolocation.getCurrentPosition(function(position) {
      sessionStorage.setItem('latlong', JSON.stringify(position.coords));
    });
  }

  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.animation;
  }
}
