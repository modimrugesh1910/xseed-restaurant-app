const EXPRESS_SERVER = 'http://localhost:3000/';

/** Websocket Server */
export  const ws_server = 'https://determined-dubinsky-3bbcbb.netlify.com/';

/** URL strings used in API calls */
export const URL_API = {
  /**
   * Login api
   */
  LOGIN: EXPRESS_SERVER + 'login',
  LOCAL_STORAGE: {
    KEYS: {
      TOKEN: 'access-token',
      CART_DATA: 'cart-data'
    }
  },
  DATA: function(): string {
    return EXPRESS_SERVER + 'api/data';
  },
  RESTAURANT: function (id): string {
    return EXPRESS_SERVER + 'api/restaurant/' + id;
  }
};
