import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {URL_API} from './core.apis';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CommonService {
    /** Notifier code for any change detected by any subject */
    readonly successNotifier: number = 200;
    /** Notifier code for any change rejected by any subject */
    readonly failureNotifier: number = 400;

    constructor(private httpClient: HttpClient) {
    }

    saveStorageData(item, data): void {
        try {
            if (this.getLocalStorageData(item) !== null && this.getLocalStorageData(item) !== undefined) {
                const itemData = this.getLocalStorageData(item);
                if (itemData && itemData.length > 0) {
                    const pos = itemData.map(function (e) {
                        return e.schemeCode;
                    }).indexOf(data.schemeCode);
                    if (pos && pos < 0) {
                        itemData.push(data);
                        localStorage.setItem(item, JSON.stringify(itemData));
                    }
                }
            } else {
                const itemData = [];
                itemData.push(data);
                localStorage.setItem(item, JSON.stringify(itemData));
            }
        } catch (e) {
            console.error('error while parsing data');
        }
    }

    setLocalStorageData(str, data): void {
        const getData = localStorage.getItem(str);
        try {
            if (getData) {
                if (JSON.parse(getData).length === 0) {
                    localStorage.removeItem(str);
                    localStorage.setItem(str, JSON.stringify(data));
                } else {
                    if (str && data) {
                        localStorage.setItem(str, JSON.stringify(data));
                    }
                }
            } else {
                if (str && data) {
                    localStorage.setItem(str, JSON.stringify(data));
                }
            }
        } catch (e) {
            console.error('error occurred while parsing data');
        }
    }

    getLocalStorageData(item): any {
        const getData = localStorage.getItem(item);
        try {
            if (getData !== null) {
                return JSON.parse(getData);
            }
        } catch (e) {
            console.error(e);
        }
    }
}
