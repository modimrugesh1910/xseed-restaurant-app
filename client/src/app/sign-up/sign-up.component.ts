import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators, NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {URL_API} from '../core/core.apis';
import {UserService} from '../core/services/user.service';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    showSucessMessage: boolean;
    serverErrorMessages: string;

    constructor(private router: Router, private fb: FormBuilder,
                private userService: UserService, private httpClient: HttpClient) {
    }

    ngOnInit() {
        if (this.userService.isLoggedIn()) {
            this.router.navigateByUrl('/auth/table');
        }
    }

    onSubmit(form: NgForm) {
        this.userService.postUser(form.value).subscribe(
            res => {
                this.showSucessMessage = true;
                setTimeout(() => this.showSucessMessage = false, 4000);
                this.resetForm(form);
            },
            err => {
                if (err.status === 422) {
                    this.serverErrorMessages = err.error.join('<br/>');
                }
                else
                    this.serverErrorMessages = 'Something went wrong.Please contact admin.';
            }
        );
    }

    resetForm(form: NgForm) {
        this.userService.selectedUser = {
            fullName: '',
            email: '',
            password: ''
        };
        form.resetForm();
        this.serverErrorMessages = '';
    }
}

