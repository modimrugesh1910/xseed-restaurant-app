const mongoose = require('mongoose');
const _ = require('lodash');

const restaurantSchema = new mongoose.Schema({
    "Restaurant_ID": {
        type: Number,
        required: 'Restaurant ID can\'t be empty',
    },
    "Country_Code": {
        type: Number,
        required: 'Country Code can\'t be empty',
    },
    "City": {
        type: String,
        required: 'City can\'t be empty',
    },
    "Address": {
        type: String,
        required: 'Address can\'t be empty',
    },
    "Locality": {
        type: String,
        required: 'Locality can\'t be empty',
    },
    "Locality_Verbose": {
        type: String,
        required: 'Locality Verbose can\'t be empty',
    },
    "Longitude": {
        type: Number,
        required: 'Longitude can\'t be empty',
    },
    "Latitude": {
        type: Number,
        required: 'Latitude can\'t be empty',
    },
});

mongoose.model('Restaurant', restaurantSchema);
