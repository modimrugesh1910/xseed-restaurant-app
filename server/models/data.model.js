const mongoose = require('mongoose');
const _ = require('lodash');

var dataSchema = new mongoose.Schema({
    "Restaurant_ID": {
        type: Number,
        required: 'Restaurant ID can\'t be empty',
    },
    "Restaurant_Name": {
        type: String,
        required: 'Restaurant Name can\'t be empty'
    },
    "Cuisines": {
        type: String,
        required: 'Cuisines can\'t be empty'
    },
    "Average_Cost_for_two": {
        type: Number,
        required: 'Average Cost for two can\'t be empty'
    },
    "Currency": {
        type: String,
        required: 'Currency can\'t be empty'
    },
    "Has_Table_booking": {
        type: String,
        required: 'Has Table booking can\'t be empty'
    },
    "Has_Online_delivery": {
        type: String,
        required: 'Has Online delivery can\'t be empty'
    },
    "Aggregate_rating": {
        type: Number,
        required: 'Aggregate rating can\'t be empty'
    },
    "Rating_color": {
        type: String,
        required: 'Rating color can\'t be empty'
    },
    "Rating_text": {
        type: String,
        required: 'Rating text can\'t be empty'
    },
    "Votes": {
        type: Number,
        required: 'Votes can\'t be empty'
    },
});

mongoose.model('Data', dataSchema);
