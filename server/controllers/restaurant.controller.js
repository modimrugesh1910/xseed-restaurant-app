const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');

const Restaurant = mongoose.model('Restaurant');

module.exports.getByRestaurant = (req, res, next) => {
    Restaurant.find({},
        (err, Data) => {
            if (!Data)
                return res.status(404).json({status: false, message: 'Data record not found.'});
            else {
                let filterData = [];
                if(Data.length !== 0) {
                    filterData = Data.filter(data => {
                        if (data['Restaurant_ID'] == req.params.restaurantId)
                            return data;
                    });
                }
                delete filterData[0]['_id'];
                return res.status(200).json({
                    status: true,
                    data: filterData
                });
            }
        });
};
