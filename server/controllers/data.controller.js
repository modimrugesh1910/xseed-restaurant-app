const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');

const Data = mongoose.model('Data');

module.exports.getAllData = (req, res, next) => {
    Data.find({},
        (err, Data) => {
            if (!Data)
                return res.status(404).json({status: false, message: 'Data record not found.'});
            else
                return res.status(200).json({
                    status: true, data: Data.map(function (item) {
                        return _.omit(item, ['_id']);
                    })
                });
        });
};

module.exports.searchData = (req, res, next) => {
    const query = { Restaurant_Name : { $regex: req.query.text, $options: "i" }};
    Data.find(query,
        (err, Data) => {
            if (!Data)
                return res.status(404).json({status: false, message: 'Data record not found.'});
            else
                return res.status(200).json({
                    status: true, data: Data.map(function (item) {
                        return _.omit(item, ['_id']);
                    })
                });
        });
};
