const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const ctrlData = require('../controllers/data.controller');
const ctrlRestaurant = require('../controllers/restaurant.controller');

const jwtHelper = require('../config/jwtHelper');

router.post('/register', ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile',jwtHelper.verifyJwtToken, ctrlUser.userProfile);
router.get('/data', ctrlData.getAllData);
router.get('/search', ctrlData.searchData);
router.get('/restaurant/:restaurantId', ctrlRestaurant.getByRestaurant);
module.exports = router;



