**XSEED FUll Stack API**
----

### Github Link - https://bitbucket.org/modimrugesh1910/xseed-restaurant-app/src

* To run apis Application
 
1. npm install
2. nodemon app.js or node app.js
 
** Root URL will be http://localhost:3000/api

* Import data into local system 

If you have MongoDB compass or robomongo you can directly import in database collection using import option

or using ubuntu/apple terminal - 

mongoimport -d XSEED_Restaurants -c data --type csv --file restaurantsa9126b3.csv --headerline

here restaurantsa9126b3.csv is downloaded given csv file on hackerearth.
 
 * To run Front-end Application 
 
 ## Development Build**
 
 1. `cd client`
 2. `npm install`
 3. `npm start`
 
 It works on http://localhost:4200/register
 
 please first register user then use login functionality it will work better.
 
 Please make sure you have installed latest angular-cli on your local machine.
 
 To install angular-cli please refer - https://angular.io/cli
 
 ## Production Build
 
 1. `npm run build`


## Work Done - 

* Upload the CSV file to the database.
* Visually interactive responsive design listing all the restaurants on the UI.
* All the data sharing between frontend and backend should be in JSON format
* Implement multiple sort features based on Rating, Votes, and Average Cost for two
* Implement functionality to search for restaurants by name, in the backend API
* Implement sign-up, log in and log out functionality. You have to create the user-auth schema in the database.
* Zip all your source code, executables, screenshots and upload the folder.
* Implement user auth, SignUp/Login functionality. 
* Collecting user’s address via asking for location permission and storing it in session-storage for further use
* Submit Screenshot, Source code & Instructions

* If you have any query shoot mail on - modimrugesh1910@gmail.com